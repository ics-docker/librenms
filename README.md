librenms
========

[LibreNMS](https://docs.librenms.org/) docker image.

This image is based on the official [php docker image](https://hub.docker.com/_/php/).

Docker pull command:

```
docker pull registry.esss.lu.se/ics-docker/librenms:latest
```
