#!/bin/sh

# To run under supervisord nfsen should not be daemononized
# nfsen is a perl script that starts several processes.
# It's not easy to adapt for supervisord.
echo "Start nfsen"
/data/nfsen/bin/nfsen start

echo "Start supervisord"
/usr/bin/supervisord
